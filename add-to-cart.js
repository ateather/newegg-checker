const axios = require('axios').default;
const exec = require('child_process').exec
const notif = require('alert');
const { exit } = require('process');

// const PRODUCT_NUMBERS = ['14-487-526', '14-137-596', '14-487-526', '14-126-455', '14-126-454',  '14-137-595', '14-137-599', '14-932-327', '14-932-328', '14-126-456', '14-487-525', '14-932-341'];
const delay = 1000 * 5;
axios.defaults.headers.common['Accept'] = 'application/json';
const instance = axios.create();

const number = process.argv[2];

function attemptAdd() {
    instance({
        url :`https://www.newegg.com/api/Add2Cart`,
        method: 'POST',
        headers : {
            'Content-Type': 'application/json',
            'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38'
        },
        data: {
            "ItemList": [{ 
                "ItemGroup": "Single", 
                "ItemNumber": number, 
                "Quantity": 1, 
                "OptionalInfos": null, 
                "SaleType": "Sales" 
            }], 
            "CustomerNumber": "x"
        }
    }).then(response => {
        if (response && response.data) {
            const { Result } = response.data

            if (Result != "Failed") {
                openCart()
            } else {
                addError(response.data)
            }
        } else {
            addError(response.data)
        }
    })
}

function addError(data) {
    console.error(data)
}

function openCart() {
    const cmd = 'start "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" https://secure.newegg.com/shop/cart'
    exec(cmd)
    //exit()
}

console.log(process.argv);
attemptAdd();
//setInterval(attemptAdd, delay);