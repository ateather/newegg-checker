const axios = require('axios').default;
const exec = require('child_process').exec
const notif = require('alert')

// You can log these in the 'Network' Tab of any browser
const PRODUCT_NUMBERS = ['14-932-340', '14-137-596', '14-487-526', '14-126-455', '14-126-454',  '14-137-595', '14-137-599', '14-932-327', '14-932-328', '14-126-456', '14-487-525', '14-932-341'];
const delay = 1000 * 5;
axios.defaults.headers.common['Accept'] = 'application/json';
const instance = axios.create()

let index = 0;


function checkInventory() {
    index = index >= PRODUCT_NUMBERS.length ? 0 : index
    const number = PRODUCT_NUMBERS[index]

    instance({
        url :`https://www.newegg.com/product/api/ProductRealtime?ItemNumber=${number}&RecommendItem=&BestSellerItemList=&IsVATPrice=true`,
        method: 'GET',
        headers : {
            'Content-Type': 'application/json',
            'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36 Edg/86.0.622.38'
        }
    })
    .then(response => {
        if (response.status != 200) {
            console.error("Network Error: ", response.status, response.statusText)
        } else {
            const { Qty, Stock, Instock } = response.data.MainItem 
            const itemNumber = response.data.MainItem.Item || number
            console.log(`${number} [${response.statusText}] Instock: `, Instock)

            // QTY gets populated before the stock is available for purchase.
            // So you can enable for ample heads-up, but its gonna be 1-24 hours before QTY turns to stock
            if (/*Qty > 0 || */Stock > 0 || Instock == true) {
                const url =  `https://www.newegg.com/p/${itemNumber}?item=${itemNumber}&Tpk=${itemNumber}`
                console.warn("OH HECK GET IT: " + number, url, Qty, Stock, Instock)
                notif("OH HECK GET IT: " + url)
            }

        }
    })
    .catch(error => console.error(error))
    
    index++;
};

setInterval(checkInventory, delay);



