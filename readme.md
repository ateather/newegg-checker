# Setup
Requires Node.Js.

Gather a list of newegg product ids via your browsers network tan and refreshing the desired product page.
In `index.js`, Populate the array `PRODUCT_NUMBERS` with the product ids.
Run `npm i` to install dependencies.

# Running
`npm start`
You will get a popup with the link to the stocked product, but it is easier to click the console link imo.